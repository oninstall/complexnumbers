<?php namespace Model\Interfaces;
// Интерфейс для получения и обновления комплексного числа.
interface IComplexModel
{
	/**
	 * findComplexNumber получает из таблицы комплексное число.
	 * @return Object complexNumber real and imaginary part of a complex number.
	 
	 */
	public function findComplexNumber();
    
	/**
	 * updateComplexNumber обновляет комлексное число в таблице. 
	 * @param Complex $complex real and imaginary part of a complex number.
	 
	 */
	public function updateComplexNumber(Complex $complex);
}