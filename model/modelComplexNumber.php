<?php namespace ComplexNumber\model;

// ModelComplex - обертка для возможного использования разных методов чтения и хранения данных.

use ComplexNumbers\library\Complex
use ComplexNumbers\model\ModelFile
use ComplexNumbers\model\ModelSQL
 /**
  * @property IOrderSource $source устанавливает, какой класс будет использован для чтения и обновления данных.
  */
class ModelComplexNumber
{
	protected $source;
	/**
	 * setSource сеттер для $source.
	 * @param IOrderSource $source.
	 
	 */
	public function setSource(IOrderSource $source)
	{
		$this->source = $source;
	}
    public function findComplexNumber(){
		return $this->source->findComplexNumber();
	}
    
	public function updateComplexNumber(Complex $complex){
	}
}