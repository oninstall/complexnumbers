<?php namespace Model;

// Модель - абстрактный класс для работы с базой данных

use \PDO;
 /**
  * @property PDO $connection 
  */
abstract class Model
{
    protected $connection;
    /**
	 * __construct устанавливает свойство $connection.
	 * @param PDO $connection used to connect to a database
	 * @author patenkoss@gmail.com
	 
	 */
    public function __construct(PDO $connection = null) {
    }
}