<?php namespace ComplexNumber\controller;

// GetDataConsole - отвечает за получение комлексного числа через консоль($argv).

use ComplexNumbers\library\Complex
use ComplexNumber\controller\Interfaces\IGetData

class GetDataConsole implements IGetData
{
	/**
	 * getComplexNumber получает комплексное число из параметров консоли..
	 * @return Complex complexNumber real and imaginary part of a complex number.
	 
	 */
    public function getComplexNumber(){
	}

}