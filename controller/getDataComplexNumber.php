<?php namespace ComplexNumber\controller;

// getDataComplex - обертка для возможного использования разных методов получения данных от пользователя.
use ComplexNumber\controller\GetDataConsole
use ComplexNumber\controller\GetDataHTTP
 /**
  * @property IGetData $source устанавливает, какой класс будет использован для получения данных.
  */
class GetDataComplexNumber
{
	protected $source;
	/**
	 * setSource сеттер для $source.
	 * @param IGetData $source.
	 
	 */
	public function setSource(IGetData $source)
	{
		$this->source = $source;
	}
    public function getComplexNumber(){
		return $this->source->getComplexNumber();
	}
}