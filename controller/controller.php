<?php namespace ComplexNumber\controller;

// Абстрактный класс для контроллеров.

abstract class Controller {
	
	/**
	 * template выводит нухный шаблон с переменными.
	 * @param string $path path to the template.
	 * @param array $vars array with variables for the template.
	 
	 */
	function template($path, array vars) {
	
	}
}