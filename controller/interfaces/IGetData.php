<?php namespace ComplexNumber/controller/Interfaces;
// Интерфейс получения комплексного числа.
interface IGetData
{
	/**
	 * getComplexNumber получает комплексное число.
	 * @return Complex complexNumber real and imaginary part of a complex number.
	 
	 */
	public function getComplexNumber();

}