<?php namespace ComplexNumber\controller;

// Главный контроллер
use ComplexNumbers\library\Complex
use ComplexNumbers\library\ComplexOperation
use ComplexNumber\controller\GetDataComplexNumber
use ComplexNumber\controller\GetDataConsole
use ComplexNumber\controller\GetDataHTTP
use ComplexNumbers\model\ModelComplexNumber
use ComplexNumbers\model\ModelFile
use ComplexNumbers\model\ModelSQL

class ControllerComplexNumber extends Controller 
{
	/**
	 * index Используя другие классы получает комплексное число пользователя, читает число из хранилища,  
	 * умножает числа, обновляет хранилище, выводит результат.
	 */
	public function index() {
	}
	
}