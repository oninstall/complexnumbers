<?php namespace ComplexNumber/controller;

// GetDataHTTP - отвечает за получение комплексного числа через GET/POST.

use ComplexNumbers/library/Complex
use ComplexNumber/controller/Interfaces

class GetDataHTTP implements IGetData
{
    /**
	 * getComplexNumber получает комплексное число из массива $_POST/$_GET.
	 * @return Complex complexNumber real and imaginary part of a complex number.
	 
	 */
    public function getComplexNumber(){
	}
}