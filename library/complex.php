<?php namespace ComplexNumbers\library;

// Complex - класс для создания комлексного числа.

 /**
  * @property float  $real real part of the complex number
  * @property float  $im imaginary part of a complex number.
  */
class Complex
{
    private $real;

    private $im;
	/**
     * __construct конструктор класса
     * @param float $real Real part of the number
     * @param float $im Imaginary part of the number
     * @return object Complex 
     */
	public function __construct ($real, $im) {
	}
    /** 
     * Переобразует объект комлексного числа в строку
     * @return string 
     * @access public
     */

    public function __toString() {
    }
	/**
     * Возвращает действительную часть комлексного числа
     * @return float 
     */
	public function getReal() {
        return $this->_real;
    }
	/**
     * Возвращает мнимую часть комлексного числа
     * @return float 
     */

    function getIm() {
        return $this->_im;
    }
}